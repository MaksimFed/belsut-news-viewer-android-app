package com.fedzianin.maksim.belsutnewsviewer.model;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class NewsOverviewParser {
    public static List<NewsItem> getParsedResponse(String response) {
        List<NewsItem> newsItems = new ArrayList<>();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(response.getBytes());

        try {
            DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = documentBuilder.parse(byteArrayInputStream);

            NodeList items = document.getElementsByTagName("item");
            for (int i = 0; i < items.getLength(); i++) {
                NewsItem newsItem = new NewsItem();
                NodeList itemNodes = items.item(i).getChildNodes();
                for (int j = 0; j < itemNodes.getLength(); j++) {
                    if (itemNodes.item(j).getNodeName().equals("title")) {
                        newsItem.setTitle(itemNodes.item(j).getTextContent());
                    }
                    if (itemNodes.item(j).getNodeName().equals("link")) {
                        newsItem.setLink(itemNodes.item(j).getTextContent());
                    }
                    if (itemNodes.item(j).getNodeName().equals("guid")) {
                    }
                    if (itemNodes.item(j).getNodeName().equals("description")) {
                        org.jsoup.nodes.Document descriptionHTML = Jsoup.parse(itemNodes.item(j).getFirstChild().getTextContent());
                        Elements image = descriptionHTML.getElementsByTag("img");
                        Elements descriptionText = descriptionHTML.getElementsByTag("p");
                        newsItem.setImage(image.attr("src"));
                        newsItem.setDescription(descriptionText.text());

                    }
                    if (itemNodes.item(j).getNodeName().equals("category")) {
                        newsItem.setCategory(itemNodes.item(j).getTextContent());
                    }
                    if (itemNodes.item(j).getNodeName().equals("pubDate")) {
                        DateFormat dateFormat = new SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z",
                                Locale.ENGLISH);
                        Date date = null;
                        try {
                            date = dateFormat.parse(itemNodes.item(j).getTextContent());
                            newsItem.setPubDate(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                newsItems.add(newsItem);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            byteArrayInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return newsItems;
    }
}
