package com.fedzianin.maksim.belsutnewsviewer.presenter;

import com.fedzianin.maksim.belsutnewsviewer.model.NewsItem;
import com.fedzianin.maksim.belsutnewsviewer.model.NewsStorage;
import com.fedzianin.maksim.belsutnewsviewer.view.MainActivity;

import java.util.List;

public class MainPresenter {

    private MainActivity activity;
    private NewsStorage storage;

    public MainPresenter(MainActivity activity) {
        this.activity = activity;
        storage = NewsStorage.getInstance();
    }

    public void populateView() {
        activity.addNews(storage.getNewsOverviews(0, MainActivity.NEWS_PER_LOAD));

    }

    public void loadMoreNewsRequested() {
        List<NewsItem> additionalNews = storage.getNewsOverviews(activity.getTotalItemCount(),
                MainActivity.NEWS_PER_LOAD);
        if (additionalNews != null) {
            activity.addNews(additionalNews);
        }
    }
}
