package com.fedzianin.maksim.belsutnewsviewer.model;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class NewsItem {
    private String title;
    private String link;
    private String description;
    private String image;
    private String category;
    private Date pubDate;

    public NewsItem() {
    }

    public NewsItem(String title, String link, String description, String image, String category, Date pubDate) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.image = image;
        this.category = category;
        this.pubDate = pubDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public String getPubDateAsString() {
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG, Locale.getDefault());
        return dateFormat.format(pubDate);
    }

    @Override
    public String toString() {
        return "NewsItem{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", description='" + description + '\'' +
                ", image='" + image + '\'' +
                ", category='" + category + '\'' +
                ", pubDate='" + pubDate + '\'' +
                '}';
    }
}
