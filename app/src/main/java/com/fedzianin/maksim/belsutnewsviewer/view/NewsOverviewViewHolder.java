package com.fedzianin.maksim.belsutnewsviewer.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.fedzianin.maksim.belsutnewsviewer.R;

import java.io.InputStream;
import java.util.concurrent.ExecutionException;

public class NewsOverviewViewHolder extends RecyclerView.ViewHolder {
    private CardView cardView;
    private TextView title;
    private TextView link;
    private TextView description;
    private TextView image;
    private TextView category;
    private TextView pubDate;
    private ImageView imageView;

    public NewsOverviewViewHolder(View itemView) {
        super(itemView);

        cardView = itemView.findViewById(R.id.item);
        title = itemView.findViewById(R.id.item_title);
        //link = itemView.findViewById(R.id.item_link);
        description = itemView.findViewById(R.id.item_description);
        //image = itemView.findViewById(R.id.item_image);
        //category = itemView.findViewById(R.id.item_category);
        pubDate = itemView.findViewById(R.id.item_pub_date);
        imageView = itemView.findViewById(R.id.item_img);


    }

    public CardView getCardView() {
        return cardView;
    }

    public TextView getTitle() {
        return title;
    }

    public TextView getLink() {
        return link;
    }

    public TextView getDescription() {
        return description;
    }

    public TextView getImage() {
        return image;
    }

    public TextView getCategory() {
        return category;
    }

    public TextView getPubDate() {
        return pubDate;
    }

    public void setImageView(String imageURL) {
        try {
            Bitmap bitmap = new DownloadImageTask()
                    .execute(imageURL).get();
            imageView.setImageBitmap(bitmap);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


    }

    public ImageView getImageView() {
        return imageView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

    }
}
