package com.fedzianin.maksim.belsutnewsviewer.model;

import java.util.List;

public class NewsStorage {
    private List<NewsItem> newsOverviews;
    private final String NEWS_URL = "http://www.bsut.by/news?format=feed&type=rss";

    private static final NewsStorage ourInstance = new NewsStorage();

    public static NewsStorage getInstance() {
        return ourInstance;
    }

    private NewsStorage() {
        fillStorage();
    }

    private void fillStorage() {
        ConnectionManager connectionManager = new ConnectionManager();
        connectionManager.execute(NEWS_URL);
        try {
            newsOverviews = NewsOverviewParser.getParsedResponse(connectionManager.get());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<NewsItem> getNewsOverviews() {
        return newsOverviews;
    }

    public List<NewsItem> getNewsOverviews(int startPosition, int size) {
        if ((startPosition + size) <= newsOverviews.size()) {
            return newsOverviews.subList(startPosition, startPosition + size);
        }
        return null;
    }

    public void setNewsOverviews(List<NewsItem> newsOverviews) {
        this.newsOverviews = newsOverviews;
    }
}
