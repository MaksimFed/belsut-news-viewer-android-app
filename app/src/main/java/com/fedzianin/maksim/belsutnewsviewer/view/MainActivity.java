package com.fedzianin.maksim.belsutnewsviewer.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.fedzianin.maksim.belsutnewsviewer.R;
import com.fedzianin.maksim.belsutnewsviewer.model.NewsItem;
import com.fedzianin.maksim.belsutnewsviewer.presenter.MainPresenter;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final int NEWS_PER_LOAD = 3;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private NewsOverviewItemAdapter adapter;
    private MainPresenter presenter;

    private final String ADAPTER_KEY = "ADAPTER";
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.rv_for_items);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        presenter = new MainPresenter(this);

        if (savedInstanceState != null) {
            adapter = (NewsOverviewItemAdapter) savedInstanceState.getSerializable(ADAPTER_KEY);
        } else {
            adapter = new NewsOverviewItemAdapter();
        }

        recyclerView.setAdapter(adapter);

        presenter.populateView();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) {
                    LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    visibleItemCount = layoutManager.getChildCount(); // number of item, that currently
                    //shows on screen
                    totalItemCount = layoutManager.getItemCount();
                    pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                            loading = false;
                            presenter.loadMoreNewsRequested();
                            loading = true;
                        }
                    }
                }
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(ADAPTER_KEY, adapter);
    }

    public void addNews(List<NewsItem> newsItems) {
        adapter.addNews(newsItems);
        adapter.notifyDataSetChanged();
    }

    public int getTotalItemCount() {
        return totalItemCount;
    }
}
