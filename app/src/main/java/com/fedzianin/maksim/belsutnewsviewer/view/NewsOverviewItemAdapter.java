package com.fedzianin.maksim.belsutnewsviewer.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fedzianin.maksim.belsutnewsviewer.R;
import com.fedzianin.maksim.belsutnewsviewer.model.NewsItem;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NewsOverviewItemAdapter extends RecyclerView.Adapter<NewsOverviewViewHolder>
        implements Serializable {

    private List<NewsItem> newsItems;
    private int newsToShow;
    private final int NEWS_PER_LOAD = 3;


    public NewsOverviewItemAdapter() {
        newsItems = new ArrayList<>();
    }

    public NewsOverviewItemAdapter(List<NewsItem> newsItems) {
        this.newsItems = newsItems;
    }

    @NonNull
    @Override
    public NewsOverviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_overview_item,
                parent, false);
        NewsOverviewViewHolder itemViewHolder = new NewsOverviewViewHolder(view);

        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsOverviewViewHolder holder, int position) {
        holder.getTitle().setText(newsItems.get(position).getTitle());
        //holder.getLink().setText(newsItems.get(position).getLink());
        holder.getDescription().setText(newsItems.get(position).getDescription());
        //holder.getImage().setText(newsItems.get(position).getImage());
        //holder.getCategory().setText(newsItems.get(position).getCategory());
        holder.getPubDate().setText(newsItems.get(position).getPubDateAsString());
        holder.setImageView(newsItems.get(position).getImage());

    }


    @Override
    public int getItemCount() {
        return newsItems.size();
    }

    public void setNewsToShow(int newsToShow) {
        this.newsToShow = newsToShow;
    }

    public void addNews(List<NewsItem> newsItems) {
        this.newsItems.addAll(newsItems);
    }
}
